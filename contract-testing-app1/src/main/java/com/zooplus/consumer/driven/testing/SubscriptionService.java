package com.zooplus.consumer.driven.testing;

import com.zooplus.consumer.driven.testing.gateways.Account;
import com.zooplus.consumer.driven.testing.gateways.AccountGateway;
import com.zooplus.consumer.driven.testing.model.Invoice;
import com.zooplus.consumer.driven.testing.model.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubscriptionService {

    private final AccountGateway accountGateway;

    @Autowired
    public SubscriptionService(AccountGateway accountGateway) {
        this.accountGateway = accountGateway;
    }

    public Invoice createInvoice(Subscription subscription) {
        Account account = accountGateway.getById(subscription.getAccountId());
        if ("friends".equals(account.getType())) {
            return new Invoice(account.getEmail(), 0);
        }
        return new Invoice(account.getEmail(), subscription.getSubscriptionType().getFeeInPenny());
    }
}
