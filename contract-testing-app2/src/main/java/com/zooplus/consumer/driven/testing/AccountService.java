package com.zooplus.consumer.driven.testing;

import org.springframework.stereotype.Service;

@Service
public class AccountService {

    public Account getById(String id) {
        if ("11111".equals(id)) {
            return new Account(id, "plain", "example@example.com");
        } else {
            return new Account(id, "friends", "tom@api.io");
        }
    }
}
